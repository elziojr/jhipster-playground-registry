#!/usr/bin/env bash

export CONTAINER_DEFAULT_PORT=8761

export APP=${1}
export NAMESPACE=${2}
export IMAGE=${3}
export SPRING_PROFILES_ACTIVE=${4}

echo "Usage: ./deploy.sh <namespace> <imageURL> <springProfilesActive>"
echo "Starting K8S Deploy"

mkdir -p .generated

for f in templates/*.yml
do
    touch ".generated/$(basename $f)"
    envsubst < $f > ".generated/$(basename $f)"
done
kubectl apply -f .generated/
