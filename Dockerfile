FROM openjdk:8-jre-alpine

ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS \
    JAVA_OPTS="" \
    SPRING_PROFILES_ACTIVE=prod

EXPOSE 8761

ADD jhipster-registry.war jhipster-registry.war

CMD java -jar jhipster-registry.war --spring.profiles.active=prod